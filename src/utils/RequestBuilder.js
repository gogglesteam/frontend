const base = process.env.VUE_APP_BACKEND;

// Helper class to help construct fetch requests
class RequestBuilder {
  constructor() {
    this.headers = {
      Accept: 'application/json',
    };
    this.method = 'GET';
    this.action = '';
  }
  static create(action) {
    return new RequestBuilder().useAction(action);
  }
  withHeaders(headers) {
    // Adds or modifies the headers.
    this.headers = {
      ...this.headers,
      ...headers,
    }; // Combine headers
    return this;
  }
  withAuthentication(token) {
    // Adds an authentication header with the provided token.
    this.headers.Authorization = 'Bearer ' + token;
    return this;
  }
  useAction(action) {
    // Set the action of the request.
    this.action = action;
    return this;
  }
  useBody(body) {
    // Sets the body of the request.
    this.headers['Content-Type'] = 'application/json';
    this.body = body;
    return this;
  }
  useMethod(method) {
    // Sets the HTTP method.
    this.method = method;
    return this;
  }
  async send() {
    const request = {
      method: this.method ?? 'GET',
      headers: this.headers,
    };
    if (this.body) request.body = JSON.stringify(this.body);
    const response = await fetch(base + this.action, request);
    return await response.json();
  }
}

export default RequestBuilder;
