import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Login from '../views/Login.vue';
import Nieuws from '../views/News.vue';


import Administration from '../views/Administration.vue';
import GroupList from '@/views/administration/groups/GroupList.vue';
import Trainings from '@/views/administration/attendances/Trainings.vue';
import Attendances from '@/views/administration/attendances/Attendances.vue';

import state from '@/state';
import Evaluation from '@/views/administration/evaluation/Evaluation.vue';
import Task from '@/views/administration/evaluation/Task.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login,
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
  },
  {
    path: '/administratie',
    name: 'Administratie',
    component: Administration,

  },
  {
    path: '/nieuws',
    name: 'Nieuws',
    component: Nieuws,

  },
  {
    path: '/administratie/groepen',
    name: 'Groepen',
    component: GroupList
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
];

export const redirectTo = 'Home';

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

router.beforeEach((to, from, next) => {
    state.load().then(next); // Load token en relevant user information.
});
router.beforeEach((to, from, next) => {
    // Logout isn't an url that is defined by a component, we just use a hook to capture logout requests and handle them.
    if (to.path !== '/logout') return next();
    state.clear(); // Clear state object.
    state.save().then(() => next({name: 'Login'})); // After logout send user to login page.
});
router.beforeEach((to, from, next) => {
    if (to.name === 'Login') return next(); // Login page doesn't need protection.
    return state.token ? next() : next({name: 'Login'}); // If there is a token; proceed, otherwise; redirect to login.
});
router.beforeEach((to, from, next) => {
    return to.name === 'Login' && state.token ? next({name: 'Home'}) : next();
});
export default router;
