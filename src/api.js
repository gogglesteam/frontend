import RequestBuilder from './utils/RequestBuilder';

import state from '@/state';

export function login(credentials) {
    return RequestBuilder.create('api/login')
        .useMethod('POST')
        .useBody(credentials)
        .send();
}

export async function getGroups() {
    const response = await RequestBuilder.create('api/groups')
        .withAuthentication(state.token)
        .send();
    if (!response.groups) console.error('Error in response');
    return response.groups;
}

export async function getGroup(group_id) {
    const response = await RequestBuilder.create('api/groups/' + group_id)
        .withAuthentication(state.token)
        .send();
    if (!response.group) console.error('Error in response');
    return response.group;
}

export async function getUsersOfGroup(group_id) {
    const response = await RequestBuilder.create(`api/groups/${group_id}/users`).withAuthentication(state.token).send();
    return response.users;
}
export async function getNews() {
    const response = await RequestBuilder.create(`api/nieuws`).withAuthentication(state.token).send();
    return response.news;
}

export async function getTraining(training_id) {
    const response = await RequestBuilder.create('api/trainings/' + training_id)
        .withAuthentication(state.token)
        .send();
    return response.training;
}

export async function getAttendances(training_id) {
    const response = await RequestBuilder.create(`api/trainings/${training_id}/attendances`)
        .withAuthentication(state.token)
        .send();
    return response.users;
}

export async function storeAttendances(training_id, attendances) {
    const response = await RequestBuilder.create(`api/trainings/${training_id}/attendances`)
        .withAuthentication(state.token)
        .useMethod('POST')
        .useBody(attendances)
        .send();
    return response.users;
}

export async function getTrainings() {
    const response = await RequestBuilder.create('api/trainings')
        .withAuthentication(state.token)
        .send();
    return response.trainings;
}
