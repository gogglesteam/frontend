import localforage from 'localforage';

// Stores the data that is shared between components. Examples are: the api-token, profile data, permissions, ...
class State {
    #isLoaded = false;
    token = null;
    id = null;
    // Load state, will be called by the root Vue instance.
    async load() {
        if (this.#isLoaded) return; // Ensure loading is called only once.
        this.token = await localforage.getItem('token');
        this.id = await localforage.getItem('user_id');

        console.log(`Token: ${this.token}`);
        this.#isLoaded = true;
    }
    // Save state, will be called by the root Vue instance.
    async save() {
        await localforage.setItem('token', this.token);
        await localforage.setItem('user_id', this.id);
    }

    clear() {
        this.token = null;
        this.id = null;
    }
}

const state = new State();

export default state;
