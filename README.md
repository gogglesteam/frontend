# Goggles frontend
## !!! IMPORTANT !!!
After each new merge, run at least these tasks:
- Install dependencies
```
npm install
```

## Getting started

### Install vue-cli
```
npm install -g @vue/cli
npm install
```

### Add .env.local
```
cp .env.local.example .env.local
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
